# Introduction

This is a template of a custom Linux kernel for Void Linux. You can edit this template to install custom kernels which are not in the repository pool. For Example: Xanmod, Zen, Linux-Surface.

# Usage

## 1. Cloning the repository

First of all, clone the repository.

```
git clone https://gitlab.com/th4tkh13m/voidcustomk.git
```

## 2. Customizing
### Template
First of all, the template file contains many important information of the kernel:
- The package name `pkgname`: The default package name is `linux5.10-custom`, you can change this into another name, let say `linux5.10-zen`. Note that when you change the `pkgname`, you must also rename the folder itself, to `linux5.10-zen` and its corresponding `dbg` and `headers`.
- Version `version`: This is the current kernel version. 
- Revision number `revision`: Since `pkgver` of `xbps-src` is `foo-0.1_1`, the revision should be a number.
- Kernel directory `wrksrc`: This should be the name of your kernel and the same of your distfile. For example, `xanmod`: `linux-${version}-xanmod${_xanmod}`
- Distfile and checksum: Please make sure to check your distfile and checksum.

### Patches
Then check the content of the `linux5.10-custom/patches` folder. If you want to add or remove any patches, feel free to do it. The patches in that folder are Void's defaults. 

### Config file
Also check the `linux5.10/files` folder. They consist the config file for the kernel. You may want to place your custom config file here and rename it as `ARCH-dotconfig-custom` denpending on your computer's architecture. For example, the custom config file of x86_64 architecture PC should be `x86_64-dotconfig-custom`.

## 3. Copying files
After that, clone the [`void-packages`](https://github.com/void-linux/void-packages) fron Void Linux repository and install `binary-bootstrap` if you have not done that yet. For more information, check the [`void-packages`'s README'](https://github.com/void-linux/void-packages/blob/master/README.md) 

Finally, copy the template folder and all the symlinks to `void-packages/srcpkgs/`:
```
cp -r linux5.10-custom-* /path/to/srcpkgs/
```
## 4. Building the kernel

Before the build, please refer to Void's [README](https://github.com/void-linux/void-packages/blob/master/README.md) 

Tips: You may want to tune the flags of the compiler so that it will compiles faster.

Build the kernel using the following command.
```
./xbps-src pkg linux5.10-custom linux5.10-custom-headers
```
## 5. Installation

After the compilation, install the built packages:
```
sudo xbps-install -R hostdir/binpkgs linux5.10-custom linux5.10-custom-headers
```

After the installation, you will notice that the revision of the custom kernel will be 2. Which means the vmlinuz will be `vmlinuz-${version}_2` and the initramfs will `initramfs-${version}_2.img`
## 6. Uninstall
The custom kernels are not the denpendencies of any Void's system packages, so that they can be easily remove:
```
sudo xbps-remove -R linux5.10-custom linux5.10-custom-headers
```

After that, use `vkpurge` to remove the leftover `vmlinuz` and `initramfs`:

List all the version:
```
sudo vkpurge list
```

Then remove the the kernel that have `_2`:
```
sudo vkpurge rm your.version.here_2
```




